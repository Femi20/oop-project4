#include "song.h"


#ifndef _PLAYLIST_H
#define _PLAYLIST_H


class Playlist
{
	private:
		Playlist& operator=(const Playlist& p);
		Song* songList;
		int current;
		int max;
		void Resize();
	public:
		Playlist();
		~Playlist();
		void addSong(const char* t, const char* a, char s, int sz);
		void removeSong(const char* name);
		void viewPlaylist() const;
		bool findSong(const char* name);
		int totalSize() const;
		void showSong(const char* name);
		bool findArtist(const char* name);
		void showByCategory(char c);
};
	
#endif