#include "playlist.h"

#include <iostream>
#include <iomanip>
#include <cctype>
#include <cstring>

using namespace std;

Playlist::Playlist()
{
	current = 0;
	max = 5;
	songList = new Song[max];
}

Playlist::~Playlist()
{
	delete [] songList;
}


Playlist& Playlist:: operator=(const Playlist& p)
{
	if(this != &p)
	{
		delete [] songList;
		max = p.max;
		current = p.current;
		songList = new Song[p.max];
		for(int i = 0; i < p.current; i++)
		{
			songList[i] = p.songList[i];
		}
	}
	
	return *this;
}		
void Playlist::Resize()
{
	
	max += 5;
	cout << "\t\tThe array is being resized to " << max << " allocated slots." << endl;
	
	Song* temp = new Song[max];
	
	for(int i = 0; i < current; i++)
	{
		temp[i] = songList[i];
	}
	delete [] songList;
	
	songList = temp;
}
	


void Playlist::addSong(const char* t, const char* a, char s, int sz)
{
	if(current == max)
	{
		Resize();
	}
	
	Style category;
	
	
	
	switch(s)
	{
		case 'p':	category = POP;			break;
		case 'r':	category = ROCK;		break;
		case 'a':	category = ALTERNATIVE;	break;
		case 'c':	category = COUNTRY;		break;
		case 'h':	category = HIPHOP;		break;
		case 'y':	category = PARODY;		break;
	}
	
	songList[current++].Set(t,a,category,sz);
}
 
void Playlist::removeSong(const char* name)
{
	int valid = findSong(name);
	
		if(valid == -1)
		{
			cout << name << "was not found in the playlist.\n\n";
		}
		else
		{
			for(int i = valid; i < current; i++)
				{
					songList[i-1] = songList[i];
				}
			current--;
			cout << "Song removed\n\n";
		}
	}
	

void Playlist::viewPlaylist() const
{
	if(current == 0)
		cout << "Playlist is empty\n\n" << endl;
	
	else
	{
		cout<< left;
		cout << "\n" << setw(35) << "Song" << setw(20) << "Artist(s)"  <<" \t\tGenre\t\tSize(MB)\n\n";
		for(int i = 0; i < current; i++)
		{
			cout << songList[i];
		}
	}
}

bool Playlist:: findSong(const char* name) 
{
	for(int i = 0; i < current; i++)
	{
		if(strcmp(songList[i].GetTitle(), name) == 0)
		{
			return true;
		}
	}
	return false;
}

bool Playlist:: findArtist(const char* name) 
{
	for(int i = 0; i < current; i++)
	{
		if(strcmp(songList[i].GetArtist(), name) == 0)
		{
			return true;
		}
	}
	return false;
}
 

void Playlist::showSong(const char* name) 
{
	for(int i = 0; i < current; i++)
	{
		if(strcmp(songList[i].GetArtist(), name) == 0)
			cout << songList[i];
			
	}
		

	for(int j = 0; j < current; j++)
	{
		if(strcmp(songList[j].GetTitle(), name) == 0)
			cout << songList[j];
			
	}
	
	
	if(findSong(name) == false && findArtist(name) == false)
		cout << "Song is not in the playlist\n\n";
	
}
	

void Playlist :: showByCategory(char c)
{
	double categorySize = 0.0;
	Style s;
	
	switch (c)
	{
		case 'p': 	s = POP;		break;
		case 'r':	s = ROCK;		break;
		case 'a':	s = ALTERNATIVE; break;
		case 'c':	s = COUNTRY;	break;
		case 'h':	s = HIPHOP;		break;
		case 'y':	s = PARODY;		break;
	}
	
	for(int i = 0; i < current; i++)
	{
		if(songList[i].GetCategory() == s){
			cout << songList[i];
			categorySize += songList[i].GetSize();
		}
		
	}
	
	if(categorySize > 0)
	{
		cout << setprecision(2);
		categorySize/100;
		cout << "Total size of category: " << categorySize << "MB/n";
	}
	else
		cout << "No song matches this category\n\n";
		
}

int Playlist::totalSize() const
{
	int totalSize = 0;
	
	for(int i = 0; i < current; i++)
	{
		totalSize += songList[i].GetSize();
	}
	
	return totalSize;
}
