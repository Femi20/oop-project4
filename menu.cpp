#include "playlist.h"
#include <iostream>
#include <iomanip>
#include <cctype>

using namespace std;

void ShowMenu()
{
		cout << "\n\tA:  \tAdd a song to the playlist";
		cout << "\n\tF:  \tFind a song on the playlist";
		cout << "\n\tD:  \tDelete a song from the playlist";
		cout << "\n\tS:  \tShow the entire playlist";
		cout << "\n\tC:  \tCategory summary";
		cout << "\n\tZ:  \tShow playlist size";
		cout << "\n\tM:  \tShow Menu\n";
		cout << "\n\tX:  \tExit the program" << endl;
}

char getChar(const char* prompt)
{
	char choice;
	
	cout << prompt;
	
	cin >> choice;
	
	choice = tolower(choice);
	cin.get();
	
	return choice;
}

bool legal(char c)
{
	return ((c == 'a') || (c == 'f') || (c == 'd') || (c == 's') ||
			(c == 'c') || (c == 'z') || (c == 'z') || (c == 'm') ||
			(c == 'x'));
}


bool legalCat(char c)
{
	return ((c == 'p') || (c == 'r') || (c == 'a') || (c == 'c') || (c == 'h') || (c == 'y'));
}


char getChoice()
{
	char choice = getChar("\n\n>>");
	
	while(!legal(choice))
	{
		cout << "\nInvalid menu choice, please try again";
		ShowMenu();
		choice = getChar("\n\n>>");
	}
	
	return choice;
}	

int main()
{
	Playlist p1;
	
	ShowMenu();
	
	char choice;
	char title[35];
	char artist[20];
	char category;
	unsigned int size;

	
	
	do{	
		choice = getChoice();
		switch(choice)
		{
			case 'a':	
			{
				cout<< "Enter the title: ";
				cin.getline(title,35);
				cout << "Enter the artist(s): ";
				cin.getline(artist, 20);
				category = getChar("Enter the category {(P)op, (R)ock, (A)lternative, (C)ountry, (H)ipHop, Parod(Y)}: ");
				while(!legalCat(category))
				{
					cout << "Invalid Category. Please try again";
					category = getChar("\n>>");
				}
				
				cout << "Enter the size: ";
				cin >> size;
				while(size <= 0)
				{
					cout << "Must enter a positive size. Please re-enter:";
					cin >> size;
				}	
				p1.addSong(title, artist, category, size);
				break;
			}
			
			case 'f':
			{
				
				cout << "Enter the song or artist name: ";
				cin.getline(title, 35);

				p1.showSong(title);
				break;
			}
			
			case 'd':
			{
				cout << "Enter the song name to delete: ";
				cin.getline(title, 35);
				
				p1.removeSong(title);
				break;
			}
			
			case 's':
			{
				p1.viewPlaylist();
				break;
			}  
			
			case 'c':
			{
				char cat = getChar("Enter the category {(P)op, (R)ock, (A)lternative, (C)ountry, (H)ipHop, Parod(Y)}: ");
				
				p1.showByCategory(cat);
				break;
			}
			case 'z':
			{
				int total = p1.totalSize();
				cout << "Total size of playlist is: " << total << "KB\n\n";
				break;
			}
			case 'm':
			{
				ShowMenu();
				break;
			}
		}		
	
	}while(choice != 'x');
 
		return 0;
}
				
				
		