#include "song.h"
#include <iomanip>
#include <cstring>

using namespace std;

ostream& operator<<(ostream& os, const Song& s)
{
	double size = double(s.GetSize())/100;
	char category[20];
	
	
	switch(s.GetCategory())
	{
		case 0:	strcpy(category, "Pop");		break;
		case 1:	strcpy(category, "Rock");		break;
		case 2:	strcpy(category, "Alternative"); break;
		case 3: strcpy(category, "Country");	break;
		case 4:	strcpy(category, "HipHop");	break;
		case 5:	strcpy(category, "Parody");	break;
	}
	
	os << fixed << setprecision(2) << left;
	os << setw(35) << s.GetTitle() <<setw(20) << s.GetArtist() << "\t\t\t" << category
		<<  "\t\t" << size << endl;
		
	return os;
		
}

Song::Song()
{
	strcpy(title, "");
	strcpy(artist, "");
	size = 0;
}


void Song::Set(const char* t, const char* a, Style st, int sz)
{
	strcpy(title, t);
	strcpy(artist, a);
	category = st;
	size = sz;
}

const char* Song::GetTitle()const
{
	return title;
}

const char* Song:: GetArtist()const
{
	return artist;
}

int Song::GetSize()const
{
	return size;
}

Style Song::GetCategory()const
{
	return category;
}







