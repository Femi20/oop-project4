menu: menu.o playlist.o song.o
	g++ -o menu menu.o playlist.o song.o
	
menu.o: playlist.h menu.cpp	
	g++ -c menu.cpp playlist.cpp

playlist.o: playlist.cpp playlist.h
	g++ -c playlist.cpp
	
song.o: song.cpp song.h
	g++ -c song.cpp
	
clean:
	rm *.o menu